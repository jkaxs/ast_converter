#!/usr/bin/python
# -*- coding: UTF-8 -*-

import sys, shutil
from datetime import datetime
from class_format_test import Files
from class_format_test import MoodleXML
from class_parser import ParseText

def process_docx():
	'''
	Обработка docx
	'''
	
	obj_file.file_name_without_ext = obj_file.randstring(15)
	file_new = '{0}{1}{2}'.format(obj_file.base_path_subdir, obj_file.file_name_without_ext, obj_file.file_extension)
	shutil.copyfile(obj_file.base_path_file, file_new)
	obj_file.base_path_file = file_new
	
	obj_file.zip_format()
	global obj_text
	obj_text = ParseText(obj_file)
	#print('OBJ TEXT_______________\n')
	#print(obj_text.__dict__)
	obj_text.declare_astdocx_input()
	
def process_html():
	''''
	Обработка html
	'''
	
	obj_file.target_resource = obj_file.base_path_file
	global obj_text
	obj_text = ParseText(obj_file)
	#print('OBJ TEXT_______________\n')
	#print(obj_text.__dict__)
	obj_text.parse_html(obj_file.target_resource)

def total_func():
	'''
	Общие функции, сборка теста
	'''
	name_test = 'Test {0}'.format(datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M"))

	#blocks = obj_text.split_block_all(obj_text.parsed_text)
	#blocks_V = obj_text.split_block_mark(blocks, r'V[\d]:')
	
	blocks_V = obj_text.set_block_structure()
	#print('BLOCK V____________________\n',blocks_V)
	
	#if not blocks_V:
	##	blocks.insert(0,{'V2:' : name_test})
	#	blocks_V = []
	#	blocks_V.append(blocks)
		
	list_cat = []
	
	moodlexml = MoodleXML(name_test, obj_file)
	for v in blocks_V:
		category = obj_text.get_from_dict(v.pop(0), 'value')
		moodlexml.put_category(category)
		block_I = obj_text.split_block_mark(v, r'I:')
		
		i = 0
		for block_quest in block_I:
			#print(i, block_quest, '\n\n')
			bl = obj_text.set_quest_type(block_quest)
			if bl:
				i += 1
				end_bl = obj_text.format_endblock(bl, str(i).zfill(3))
				
				if end_bl:
					#print(end_bl, '\n\n')

					moodlexml.process_data(end_bl, category[0])
					list_cat.append(moodlexml.quiz)
					
			
		
		''''
		i = 0
		for block_quest in blocks_I:
			bl = obj_text.set_quest_type(block_quest)
			if bl:
				i += 1
				end_bl = obj_text.format_endblock(bl, str(i).zfill(3))
				
				if end_bl:
					moodlexml.process_data(end_bl, category[0])
					list_cat.append(moodlexml.quiz)
		'''
	moodlexml.write_xml()
	
def finale():
	'''
	Финал: очистка, вывод логов и т.п.
	'''
	if obj_text.MODE == '.docx':
		print('{0}{1}.zip'.format(obj_file.base_path_subdir, obj_file.file_name_without_ext))
		print('{0}{1}'.format(obj_file.base_path_subdir, obj_file.file_name_without_ext))
		obj_file.file_del('{0}{1}.zip'.format(obj_file.base_path_subdir, obj_file.file_name_without_ext))
		obj_file.file_del('{0}{1}'.format(obj_file.base_path_subdir, obj_file.file_name_without_ext))

	print('The end')
	
if __name__ == '__main__':
	try:

		if len (sys.argv) < 2:
			raise SystemExit
		path = sys.argv[1]

		#path = 'file-input/static/upd_var5.docx'
		obj_file = Files(path)
		print(obj_file.__dict__)

		process_file = {
			'.docx' : process_docx,
			'.html' : process_html,
			'.total' : total_func, # общие функции
			'.clear' : finale, # общие функции
		}
		
		process_file[obj_file.file_extension]()
		process_file['.total']()
		process_file['.clear']()
		
		

	except SystemExit:
		print('Экстренное завершение')

	'''	
	d = { '.docx' : ( obj_file.testfunc1, obj_file.testfunc2 ),
		'.html' : ( obj_file.testfunc2, )
	}
	
	for func in d['.docx']:
		func('args')
	'''
