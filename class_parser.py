#! /usr/bin/env python
# -*- coding: utf-8 -*-
import time
import re
import base64
import os
import subprocess
import copy
from PIL import Image, ImageDraw
import lxml.etree as etree
import lxml.html as lhtml

from wand.image import Image
from wand.display import display
from docx import Document
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH
from bs4 import BeautifulSoup

class ParseText():
	'''
	--------------------------------------------------------------------
	Парсер документа docx|html
	
	Итог вида: [
					{'answers_ball': {'-': int, '+': int},
					 'question_type': 'str',
					 'question_test': 'str',
					 'question_test': 'str',
					 'answers_text': [()]
				]
	--------------------------------------------------------------------
	'''


	def timer(f):
		'''
		Время выполнения функций
		'''
		def tmp(*args, **kwargs):
			t = time.time()
			res = f(*args, **kwargs)
			print("Время выполнения функции: %f" % (time.time()-t))
			return res

		return tmp
		

	@timer
	def __init__(self, oFiles, mode = 'DOCX', empty_negative_answers = True):
		'''
		empty_negative_answers:
			True: в отрицательных ответах ставится 0
			False: в отрицательных ответах ставится -100/n отрц. ответов
		----------------------------------------------------------------
		* return: list	 
		'''
		self.ext_supported = ['.html', '.docx']
		self.MODE_CMD = 1
		
		self.Files = oFiles
				
		if self.Files.file_extension in self.ext_supported:
			self.MODE = self.Files.file_extension
		else:
			self.Files.put_log('Тип файла не поддерживается: "{0}"'.format(self.Files.file_extension))
			raise SystemError
		
		self.EMPTY_NEGATIVE_ANSWERS = empty_negative_answers
		self.QUESTION_BALL = 1
		
		#struct dirs docx file
		self.path_word = ''
		self.path_documentxml = ''
		self.path_documentxml_rels = ''
		
		#для поиска маркеров (используется match, а не search)
		self.quest_regex = re.compile(r'V[\d]:|I:|Q:|S:|B:')
		self.ans_regex = re.compile(r'\+:|-:|R[\d]:|L[\d]:')
		self.V_regex = re.compile(r'V[\d]')
		self.service_mark_regex = re.compile(r'V[\d]:|I:|B:')
	

		
		self.parsed_text = '' #данные parse_documentxml
		self.img_rels = [] 		#список с картинками [id, target(full path)]
		self.img_unsupported = ('.wmf', '.emf') #неподдерживаемые форматы
		self.img_size = {}
				
		#отступ от начала строки при склейке строк для избежание вида L \n : \n 12 уже не нужно
		self.text_deviation = 4
		
		self.category = 'Test(of test)'
		
		
		'''
		#struct dirs
		self.path_word = '{0}word/'.format(self.Files.path_project)
		self.path_documentxml = '{0}word/document.xml'.format(self.Files.path_project)
		self.path_documentxml_rels = '{0}word/_rels/document.xml.rels'.format(self.Files.path_project)
		
		self.parse_documentxml(self.path_documentxml)
		if not self.parsed_text:
			self.put_log('Данные из {0} не получены:'.format(self.path_documentxml))
			raise SystemExit #?
		
		#выгрузка картинок
		i = 1
		doc = etree.parse(self.path_documentxml_rels)
		for v in doc.getroot():
			img_path = self.path_word + v.attrib["Target"]
			if '.bin' not in img_path and '.xml' not in img_path:
				checking_img_path = self.check_img_format(img_path)
				self.img_rels.append((v.attrib['Id'], checking_img_path))
				print("{0}, {1}".format(i, checking_img_path))
				i += 1
		'''
		
		self.preambula = ''

		
	def declare_astdocx_input(self):
		'''
		Объявление входного AST docx
		----------------------------------------------------------------
		* Args:
		----------------------------------------------------------------
		* return: None	 
		'''
		
		#struct dirs
		self.path_word = '{0}word/'.format(self.Files.target_resource)
		self.path_documentxml = '{0}word/document.xml'.format(self.Files.target_resource)
		self.path_documentxml_rels = '{0}word/_rels/document.xml.rels'.format(self.Files.target_resource)
		
		self.parse_documentxml(self.path_documentxml)
		if not self.parsed_text:
			self.put_log('Данные из {0} не получены:'.format(self.path_documentxml))
			raise SystemExit #?
		
		#выгрузка картинок
		i = 1
		doc = etree.parse(self.path_documentxml_rels)
		for v in doc.getroot():
			img_path = self.path_word + v.attrib["Target"]
			#print(img_path, ' - \n', v.attrib["Target"])
			
			#разобраться с file:///
			if '.bin' not in img_path and '.xml' not in img_path and 'file:///' not in img_path:
				checking_img_path = self.check_img_format(img_path)
				self.img_rels.append((v.attrib['Id'], checking_img_path))
				print("{0}, {1}".format(i, checking_img_path))
				i += 1
				
	
		
	@timer
	def check_img_format(self,imgpath):
		'''
		Проверка и конвертация картинок в png
		.wmf и .emf - проблемы
		----------------------------------------------------------------
		* Args:
		----------------------------------------------------------------
		* return: str	
		
		cmd = 'convert "{0}" "{1}.png"'.format(imgpath, filename)
		cmd = 'mogrify -format png "{0}"'.format(imgpath)
		out = subprocess.call(cmd, stderr = subprocess.STDOUT,  shell=True)
		'''
		
		filename, file_extension = os.path.splitext(imgpath) #разбивка '/path/file', '.ext'
		if file_extension in self.img_unsupported:
			
			if file_extension == '.wmf' and self.MODE == '.docx':
				print('WMDDDD')
				# выходный файл
				new_imgpath = '{0}.png'.format(filename)
				print(new_imgpath)
				print(imgpath)
				print(self.Files.target_resource)
				#cmd = 'cd {0}word/media/ && convert {1} {2} && convert {2} -crop 200x100+300+500 {2}'.format(self.Files.target_resource, imgpath, new_imgpath)
				#print(cmd)
				
				#self.MODE_CMD = 1
				if self.MODE_CMD == 1:
					try:
						#cmd = 'cd {0}word/media/ && libreoffice --headless --convert-to png {1}'.format(self.Files.target_resource, imgpath)
						#cmd = 'cd {0}word/media/ && convert {1} temp.png && convert temp.png -crop 200x1123+200+0 {2} && rm temp.png'.format(self.Files.target_resource, imgpath, new_imgpath)
						#cmd = 'cd {0}word/media/ && libreoffice --headless --convert-to png {1} && convert {2} -gravity Center -crop 150x100+0+0 {2}'.format(self.Files.target_resource, imgpath, new_imgpath)
						#cmd = 'cd {0}word/media/ && libreoffice --headless --convert-to png {1} && convert {2} -transparent white -gravity Center -crop 150x100+0+0 {2}'.format(self.Files.target_resource, imgpath, new_imgpath)
						cmd = 'cd {0}word/media/ && libreoffice --headless --convert-to png {1} && convert {2} -trim +repage -transparent white {2}'.format(self.Files.target_resource, imgpath, new_imgpath)
						
						out = subprocess.call(cmd, stderr = subprocess.STDOUT,  shell=True)
						return new_imgpath
					except:
						self.Files.put_log('Проблемы с "{0}", невозможно конвертировать с помощью Wand'.format(imgpath))
						return imgpath
					
					''''
					try:
						cmd = 'cd {0}word/media/ && libreoffice --headless --convert-to png {1}'.format(self.Files.target_resource, imgpath)
						cmd = 'cd {0}word/media/ && libreoffice --headless --convert-to png {1} && convert {1} -crop 200x100+300+500 {1}'.format(self.Files.target_resource, imgpath)
						out = subprocess.call(cmd, stderr = subprocess.STDOUT,  shell=True)
						return new_imgpath
					except:
						self.Files.put_log('Проблемы с "{0}", невозможно конвертировать с помощью Wand'.format(imgpath))
						return imgpath
					'''
				else: 
					try:
						with Image(filename = imgpath) as img:
							print(img)
							img.format = 'svg'
							img.save(filename = new_imgpath)
							return new_imgpath
					except:
						self.Files.put_log('Проблемы с "{0}", невозможно конвертировать'.format(imgpath))
						return imgpath
			
			else:
				
				try:
					with Image(filename = imgpath) as img:
						print(img)
						img.format = 'png'
						img.save(filename = new_imgpath)
						return new_imgpath
				except:
					self.Files.put_log('Проблемы с "{0}", невозможно конвертировать'.format(imgpath))
					return imgpath
					
		else:
			#print(imgpath)
			return imgpath
	
	def parse_html(self,file_html):
		'''
		Скллейка строк, разбивка по разделителям из шаблонов
		----------------------------------------------------------------
		* Args: list
		----------------------------------------------------------------
		return: list	
		'''
		
		# TODO переделать открытие всё
		f = open(self.Files.target_resource)
		doc_html = ''
		for line in f:
			if '<img' in line:
				print(line)
				#print(re.findall(r'src="\w+\.\w+"', line))
				print(re.findall(r'(src=[\'"\w+\-\d+_\.]+)', line))
				imgname = re.split(r'"', re.findall(r'(src=[\'"\w+\-\d+_\.]+)', line)[0])
				res = ' @{0}{1}@ <img'.format(self.Files.base_path_subdir, imgname[1])
				line = re.sub(r'<img', res, line) 
			doc_html += line
		
		doc = lhtml.fromstring(doc_html)
		data = []
		[data.append(x.text_content()) for x in doc.xpath('//p')]
		dataProc = [" ".join(x.replace('\n', ' ').split()).strip() for x in data]
		
		self.parsed_text = []
		result_data = []
		for d in dataProc:
			#print(d, '\n')
			dict_par = self.search_block_mark(d)
			if dict_par:
				result_data.append(dict_par)
			else:
				print('elsee: ', d)
				prev_index = len(result_data) - 1
				prev_val = list(result_data[prev_index].values())[0]
				prev_key = list(result_data[prev_index].keys())[0]
				result_data[prev_index][prev_key] = '{0} <br> {1} <br>'.format(prev_val.rstrip(), d.lstrip())
		
		#sprint(result_data)
		self.parsed_text = result_data
		

	def parse_html_1(self,file_html):
		'''
		Скллейка строк, разбивка по разделителям из шаблонов
		----------------------------------------------------------------
		* Args: list
		----------------------------------------------------------------
		return: list	 
		'''
		
		#name_space = {'w':'http://schemas.openxmlformats.org/wordprocessingml/2006/main',
		#				'a':'http://schemas.openxmlformats.org/drawingml/2006/main',
		#				'v':'urn:schemas-microsoft-com:vml'		
		#				}
						
		doc = lhtml.fromstring(self.Files.target_resource)
		root = doc.getroot()
		print(doc)
		
		for d in doc:
			print(d)
		struct = doc.xpath('//img | //p')
		#struct_p = doc.xpath('//p ')
		data = []
		#print('g\n')
		
		for st in struct:
			#print(st)
			#if 'p' in st.attrib:
			#data.append(st.text_content())
			#print(data[-1])
		#	data[0]
			if 'src' in st.attrib:
				#print(st.attrib['src'])
				#print(data)
				#data[-1] += ' @{0}{1}@ '.format(self.Files.base_path_subdir, st.attrib['src'])
				#st.attrib['text'] = '@{0}{1}@'.format(self.Files.base_path_subdir, st.attrib['src'])
				text = '@{0}{1}@'.format(self.Files.base_path_subdir, st.attrib['src'])
				
				#lexer = guess_lexer(text)
				#hilited = highlight(text, lexer, HtmlFormatter())
				
				doc.replace(st, '<p></p>')
				
				print(doc)
				#print('\n\n')
				#st = "".join(new_text)
				#st.attrib['text'] = new_text
				#print(st.attrib)
				#print(st)
				#print(st.text)
				#print( '@{0}{1}@'.format(self.Files.base_path_subdir, st.attrib['src']) )
				#img = '@{0}{1}@'.format(self.Files.base_path_subdir, st.attrib['src'])
				#st = img
			#print(st)
			#print(st.text_content())
			#text = st.text_content() if st.text_content() else st
			
			#if 'p' in st.attrib:
			#if text is not None:
				
			#	text = st.text_content() + text
			#else:
			#	text = st.text_content()
			
			#try:
			#	data.append(st.text_content())
			#except AttributeError:
			#	data.append(st)
		
		for st in struct:
			print(st, st.attrib)	
		#for d in data:
		#	print(d)
		
		#for st in struct:
		#	print(st)
		#	data.append(st.text_content())
			
		#print(data)
		
		#for elt in root.iter('p'):
		#	text_result = ''
			
		#	text=elt.text_content()
			#print(elt)
			#for e in elt:
			#	print(e)
			
		#	if text:
				#data.append(text)
				#print(text)
		#		text_result += text
				
		#	for elt1 in elt.iter('img'):
				#Выгрузка картинок
		#		i = 1
		#		if elt1.attrib['src']:
					#print(elt1.attrib['src'])
					#print( self.Files.base_path_subdir + elt1.attrib['src'])
					#img_path = self.path_word + v.attrib["Target"]
				#	if '.bin' not in img_path and '.xml' not in img_path:
				#		checking_img_path = self.check_img_format(img_path)
				#		self.img_rels.append((v.attrib['Id'], checking_img_path))
				#		print("{0}, {1}".format(i, checking_img_path))
				#		i += 1
				
					#--print(elt1.attrib['src'])
					
					#data.append('@{0}{1}@'.format(self.Files.base_path_subdir, elt1.attrib['src']))
		#			text_result += '@{0}{1}@'.format(self.Files.base_path_subdir, elt1.attrib['src'])
					#print(data)
					#print('\n\n\n')
			#if text_result:
				#print(text_result)
				#
				#data.append(text_result)
		
		
		dataProc = [ " ".join(x.replace('\n', ' ').split()).strip() for x in data]
		self.parsed_text = []
		
		for d in dataProc:
			dict_par = self.search_block_mark(d)
			if dict_par:
				self.parsed_text.append(dict_par)
				
			#else:
				#print('else ' + d)
			'''	
				if dict_par:
				result_data.append(dict_par)
				else:
					prev_index = len(result_data) - 1
					prev_val = list(result_data[prev_index].values())[0]
					prev_key = list(result_data[prev_index].keys())[0]
					result_data[prev_index][prev_key] = '{0} {1}'.format(prev_val.rstrip(), str_par.lstrip())
					
				#print(str_par,'\n\n')	
				data.append(str_par)
				data.append('<br>')
			'''
			#print(self.parsed_text, '\n')
		
		#self.parsed_text = dataProc
		
	def parse_documentxml(self,file_xml):
		'''
		Скллейка строк, разбивка по разделителям из шаблонов
		
		w:p : {					| абзац
				w:pPr, 			| формат абзаца
				w:r : {			| содержимое абзаца
					  w:rPr,	| формат текста
					  w:t		| текст
					  }
				}
		----------------------------------------------------------------
		* Args: list
		----------------------------------------------------------------
		return: list	 
		'''
		
		name_space = {'w':'http://schemas.openxmlformats.org/wordprocessingml/2006/main',
						'a':'http://schemas.openxmlformats.org/drawingml/2006/main',
						'v':'urn:schemas-microsoft-com:vml',
						'wp':'http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing'
						}
						
		doc = etree.parse(file_xml)
		root = doc.getroot()
		#struct = doc.xpath('//w:t | //v:imagedata | //a:blip', namespaces = name_space) #?
		
		data = []
		result_data = []
		WP = doc.xpath('//w:p', namespaces = name_space) 
		for wp in WP:
			paragraph = []	
			WR = wp.xpath('.//w:r', namespaces = name_space)
			for wr in WR:
				
				WT = wr.xpath('.//w:t', namespaces = name_space)
				ABLIP = wr.xpath('.//v:imagedata | .//a:blip', namespaces = name_space)
				WDRAWING = wr.xpath('.//w:drawing', namespaces = name_space)
				VSHAPE = wr.xpath('.//v:shape', namespaces = name_space)
				
				if WT:
					
					#***************************************************
					# Свойства текста
					#***************************************************
					WRPR_vertAlign = wr.xpath('.//w:vertAlign', namespaces = name_space) #индекс
					WRPR_color = wr.xpath('.//w:color', namespaces = name_space) #цвет шрифта
					WRPR_highlight = wr.xpath('.//w:highlight', namespaces = name_space) #цвет фона
					WRPR_u = wr.xpath('.//w:u', namespaces = name_space) #подчеркивание
					#TODO провеку на value организовать
					WRPR_b = wr.xpath('.//w:b', namespaces = name_space) #полужирный
					WRPR_i = wr.xpath('.//w:i', namespaces = name_space) #курсив
					
					styles = []
					if WRPR_vertAlign:
						if WRPR_vertAlign[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val'] == 'subscript':
							styles.append("vertical-align: sub;")
						if WRPR_vertAlign[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val'] == 'superscript':
							styles.append("vertical-align: super;")
					
					if WRPR_color:
						if WRPR_color[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val']:
							styles.append('color:{0};'.format(WRPR_color[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val']))
														
					if WRPR_highlight:
						if WRPR_highlight[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val']:
							styles.append('background-color:{0};'.format(WRPR_highlight[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val']))
		
					if WRPR_u:
						styles.append('font-weight:bold;')
						if WRPR_u[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val']:
							styles.append('text-decoration:underline;')
					
					if WRPR_b:
						styles.append('font-weight:bold;')
						if "{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val" in WRPR_b[0].attrib and WRPR_b[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val'] == '0':
							styles.remove('font-weight:bold;')
					 
					
					if WRPR_i:
						styles.append('font-style:italic;')
						if "{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val" in WRPR_i[0].attrib and WRPR_i[0].attrib['{http://schemas.openxmlformats.org/wordprocessingml/2006/main}val'] == '0':
							styles.remove('font-style:italic;')
						
					#***************************************************
					
					if styles:
						text = '<span style = "{0}">{1}</span>'.format(''.join(styles), WT[0].text)
					else:
						text = WT[0].text
					#data.append(text) 
					paragraph.append(text) 
				
				
				# картинки лежат w:object -> v:shape, не в w:t, wp:extent
				if VSHAPE:
					
					spl = lambda patt, stri: re.split(patt, stri)
					style_shape = list(map(spl, [r':', r':'], spl(r';', VSHAPE[0].attrib['style'])))
					
					sx = [x[1] for x in style_shape if x[0] == "width"]
					sy = [x[1] for x in style_shape if x[0] == "height"]
					size = {'cx' : sx[0], 'cy' : sy[0]}
					print(size, sep='\n')


				# картинки лежат w:drawing
				if WDRAWING:
					
					#WPEXTENT = [wdrawing.xpath('.//wp:extent', namespaces = name_space) for wdrawing in WDRAWING]
					WPEXTENT = wr.xpath('.//wp:extent', namespaces = name_space)
					
					if WPEXTENT:
						atr_WPEXTENT = WPEXTENT[0].attrib
						WPEXTENT_cx = self.emu_traslate(atr_WPEXTENT['cx'])
						WPEXTENT_cy = self.emu_traslate(atr_WPEXTENT['cy'])
						#print('cx = {0}, cy = {1}'.format(WPEXTENT_cx, WPEXTENT_cy))
						size = {'cx' : WPEXTENT_cx, 'cy' : WPEXTENT_cy}
									
				if ABLIP:
					print('ABLIP')
					atr = ABLIP[0].attrib
					if '{http://schemas.openxmlformats.org/officeDocument/2006/relationships}id' in atr:
						paragraph.append('@'+atr['{http://schemas.openxmlformats.org/officeDocument/2006/relationships}id']+'@')
						self.img_size[atr['{http://schemas.openxmlformats.org/officeDocument/2006/relationships}id']] = size
					elif '{http://schemas.openxmlformats.org/officeDocument/2006/relationships}embed' in atr:
						paragraph.append('@'+atr['{http://schemas.openxmlformats.org/officeDocument/2006/relationships}embed']+"@")
						self.img_size[atr['{http://schemas.openxmlformats.org/officeDocument/2006/relationships}embed']] = size
					else:
						pass
			#абзац
			str_par = '{0} <br>'.format(''.join(paragraph))
			dict_par = self.search_block_mark(str_par)
			#print(str_par, '\n\n')	
			#print(dict_par)
			if dict_par:
				result_data.append(dict_par)
			else:
				prev_index = len(result_data) - 1
				prev_val = list(result_data[prev_index].values())[0]
				prev_key = list(result_data[prev_index].keys())[0]
				result_data[prev_index][prev_key] = '{0} {1}'.format(prev_val.rstrip(), str_par.lstrip())
				
			#print(str_par,'\n\n')	
			data.append(str_par)
			data.append('<br>')
	
		#for d in result_data: print(d,'\n-----------\n')	
		#print(self.img_size)
		self.parsed_text = result_data
		#self.parsed_text = data
	
	
	#@timer
	def emu_traslate(self, val):
		''' 
		Перевод из EMU в px
		----------------------------------------------------------------
		* Args: string
		inch = 2.54cm
		emu = 914400inch
		mm = 3.8px
		----------------------------------------------------------------
		* return: int	
		'''
		emu = int(val)
		inch = emu/914400
		mm = inch * 25.4
		px = mm * 3.8
		#px = mm * 0.26458
	
		return round(px, 2)
	
	#@timer
	def search_block_mark(self, val):
		''' 
		Разбивка по разделителям, нисходящая - т.е. от 1-го элемента и ниже
		----------------------------------------------------------------
		* Args: string
		----------------------------------------------------------------
		* return: list(dict)	
		* structure: V2: I: Q: S: -:\+:\R1:L1: 
		'''
		find_mark_quest = self.quest_regex.search(val)
		find_mark_ans = self.ans_regex.search(val)
				
		if find_mark_quest or find_mark_ans:
			mark_key = find_mark_quest.group(0) if find_mark_quest else find_mark_ans.group(0) 
			format_str = val.replace(mark_key, '')
			d = {mark_key : format_str.strip()}
	
		else:
			d = None
		return d
		
	
	def get_from_dict(self, val, flag):
		''' 
		Получить ключ/значение
		----------------------------------------------------------------
		* Args: string
		----------------------------------------------------------------
		* return: int|str
		'''
		if flag == 'key':
			return list(val.keys())[0]
		if flag == 'value':
			return list(val.values())[0]
			
	
	def clear_text(self, val, mark):
		''' 
		Очистить текст от лишних <br> в служебных марках
		----------------------------------------------------------------
		* Args: string
		----------------------------------------------------------------
		* return: int|str
		'''
		if self.service_mark_regex.match(mark):
			return val.replace('<br>', '').strip()
		else:
			return val
		
	
	def set_block_structure(self):
		'''
		Разбивка по блокам, установка свойсв вопросов.
		Так как MoodleXML делает категории вида cat1/cat2, по сути, блок V
		будет вложен только на один уровень
		Вложенность категорий у Moodle > 10, т.е. V приводится к виду:
			'V:' : 'textV1/textV2/textV3' etc
			
		Структурка следующая: 
							[
							 [
								V:cat1, 
								[{I:1}, {S:} etc], 
								[{I:2}, {S:} etc],
							 ],
							 
							  [
								V1:cat2, 
								[{I:1}, {S:} etc], 
								[{I:2}, {S:} etc],
							 ],
							]
							 
			
		TODO как быть, если лист следующего вида:
		V1:
			I: 1,etc
			I: 2,etc
		V2: I: 3. etc
		----------------------------------------------------------------
		* Args:
		----------------------------------------------------------------
		* return: None	 
		''' 
		
		list_V = []
		arr_V = []
		arr_other = []
		
		#print(self.parsed_text)
		for i, mark_val in enumerate(self.parsed_text):
			#print(i, mark_val)
			mark = self.get_from_dict(mark_val, 'key')
			val = self.clear_text(	self.get_from_dict(mark_val, 'value'), mark)
			
			# обнаружение V:	
			if self.V_regex.search(mark):
				#format_val = self.clear_format(val)
				#print(val, format_val, '\n\n')
				list_V.append(self.clear_format(val))

			else:
				if list_V:
					arr_V.append({'V:' : '/'.join(list_V)})
					list_V = []
						
				arr_V.append({mark : val})
				
		blocks_V = []
		blocks_V = self.split_block_mark(arr_V, r'V:')
		
		return blocks_V	
		
		#'''
	
	def clear_format(self, val):
		'''
		очистить формат
		----------------------------------------------------------------
		* Args: list, string
		----------------------------------------------------------------
		* return: list or None
		'''
		
		#result = re.sub(r'^<[\w\s>=\'\-":;]+>|<\/span>', '', val)
	
		return re.sub(r'<[\w\s>=\'\-":;]+>|<\/span>', '', val)
		
	#@timer
	def split_block_mark(self, arr, mark, category = ''):
		'''
		Разбивка по разделителям, нисходящая - т.е. от 1-го элемента и ниже
		----------------------------------------------------------------
		* Args: list, string
		----------------------------------------------------------------
		* return: list or None
		'''
		res_arr, sub_res_arr = [], []
		count = 0
		length_arr = len(arr)
		
		e_num = enumerate(arr)
		for k,val in e_num:
			#print(k,val, '\n\n')
			if type(val) is dict:
				mark_current = list(arr[k]) #keys
				mark_next = list(arr[k + 1]) if k < length_arr - 1 else [''] #следующая марка за исключением последнего элемента
								
				if re.match(mark, mark_current[0]):
					val_mark = val
					count += 1
				else:
					sub_res_arr.append(val)
					
				if re.match(mark, mark_next[0]) or k == length_arr - 1: #последний эелмент перед след маркером
					if count > 0: 
						sub_res_arr.insert(0, val_mark)
						res_arr.append(sub_res_arr)
						#res_arr = {}
						sub_res_arr = []
					#else:
					#	self.Files.put_log('Маркер "{0}" не найден, создание блока невозможно'.format(mark))
		
		if count > 0: 
			return res_arr
		else:
			self.Files.put_log('Маркера "{0}" не найдено. Список оставлен без изменений.'.format(mark), flag = 'log')
			return None


	#@timer
	def split_block_all(self,arr):
		'''
		Склейка строк, разбивка по разделителям из шаблонов
		!исключить
		----------------------------------------------------------------
		* Args: list
		----------------------------------------------------------------
		return: list	 
		'''
		res_arr = []
		
		#quest_regex = re.compile(r'V[\d]:|I:|Q:|S:|B:')
		#ans_regex = re.compile(r'\+:|-:|R[\d]:|L[\d]:')
		
		for k, val in enumerate(arr):
			#print(k,val)
			
			#во избежание вида L \n : \n 12
			# 4 - отступ от начала строки
			stri = ''.join(arr[k : k + self.text_deviation]) 
			#print(stri)
			#print('\n\n')
			
			matching_stri = stri.strip()
			
			find_mark_quest = self.quest_regex.match(matching_stri)
			find_mark_ans = self.ans_regex.match(matching_stri)
			
			#print(find_mark_quest)
			#print(find_mark_ans)
			
			if find_mark_quest:
				mark_key = find_mark_quest.group(0)
				#format_str = val.replace(mark_key, '')
				format_str = val.strip()
				res_arr.append({mark_key : format_str})
				key = res_arr.index({mark_key : format_str})
				
			elif find_mark_ans:
				mark_key = find_mark_ans.group(0)
				#format_str = val.replace(mark_key, '')
				format_str = val.strip()
				res_arr.append({mark_key : format_str})
				key = res_arr.index({mark_key : format_str})
				
			else:
				
				try:
					format_str += val
					res_arr[key] = {mark_key : format_str}
				except (UnboundLocalError,IndexError):
					self.Files.put_log('некорректный шаблон в месте: "{0}"'.format(val))
		
		format_arr = []
		for k,v in enumerate(res_arr):
			list_val = list(v.values())
			list_key = list(v.keys())

			if list_val[0].strip() != '': #откуда появляются?
				format_s = list_val[0].replace(list_key[0], '')
				format_arr.append({list_key[0] : format_s.strip()})
		
		
		#print(format_arr)
		#for v in format_arr:
			#print(v)
			#pass
		return format_arr #res_arr	
		
	#@timer
	def set_quest_type(self, head_block):
		'''
		Определение типа вопроса:
			1. Подсчет количества маркеров ответа
			3. Подсчет баллов + формирование блока ответа
			2. Определение типа
		----------------------------------------------------------------
		* Args: list
		----------------------------------------------------------------
		* return: list	 
		'''
		#for v in tuple(val.values()): 
		#для обновления словарей во время итерации
	
		quest_type = 'None'
		ans_ball = 'None'
				
		
		count_pl_min = self.get_count_mark(head_block,('+:','-:'))
		count_plus = count_pl_min['+:']
		count_minus = count_pl_min['-:']
		
		patt_LR = (r'L[\d]:',r'R[\d]:')
		count_LR = self.get_count_mark(head_block, patt_LR)
		count_L = count_LR['L[\d]:']
		count_R = count_LR['R[\d]:']
		#ошибки
		if count_L == 0 and count_plus == 0: #мб эссэ
			self.Files.put_log('QUESTION_ERR: Вопрос "{0}" не опознан'.format(head_block))
			return None
		elif (count_L > 0 or count_R > 0) and (count_plus > 0 or count_minus > 0):
			self.Files.put_log('QUESTION_ERR: В вопросе "{0}" несколько ответов, принадлежащих к разным типам вопроса'.format(head_block))
			return None
		
		#обработка	
		else:
			if count_plus > 0 and count_minus == 0: #shortanswer
				quest_type = 'shortanswer'
				ans_ball = self.set_ans_ball(head_block, quest_type)
				
			elif count_L > 0 and count_R > 0:
				quest_type = 'matching'
				ans_ball = self.set_ans_ball(head_block, Quest_type = quest_type, counts = count_LR)
			
			elif count_plus >0 and count_minus > 0:
				quest_type = 'multichoice'
				ans_ball = self.set_ans_ball(head_block, Quest_type = quest_type, counts = count_pl_min)
			else:
				self.Files.put_log('QUESTION_ERR: В вопросе "{0}" что-то не так'.format(head_block))
				return None	
				
			head_block.insert(0, ans_ball)
			head_block.insert(0,{'question_type' : quest_type})		
		
		return head_block

	

	#@timer
	def set_ans_ball(self, lst, Quest_type='', counts={}):
		''' 
		Определение баллов за ответ,
		В matching считаются только L, не R.
		В MoodleXML не считается вовсе
		
		----------------------------------------------------------------
		* Args: val = list, Quest_type = string  counts=list
		----------------------------------------------------------------
		* return: None	 
		'''
		counts_t = tuple(counts)
		
		if Quest_type in ('shortanswer', 'essay', 'description'):
			ball_plus = 100
			ball_minus = None
			
		# добавить проверку на нулевые +: и -:
		elif Quest_type == 'multichoice' and len(counts_t) > 1:
			if counts['+:'] == 1:
				ball_plus = 100
				ball_minus = 0 if self.EMPTY_NEGATIVE_ANSWERS == True else -100
			else:
				ball_plus = round(100/counts['+:'], 5)
				ball_minus = 0 if self.EMPTY_NEGATIVE_ANSWERS == True else round(-100/counts['-:'], 5)		
		elif Quest_type == 'matching':
			ball_plus = round(100/counts['L[\d]:'], 5)
			ball_minus = None
			
		return ({'answers_ball' : {'+' : ball_plus, '-' : ball_minus}})
		
		#lst.append({'ans_ball' : {'+' : ball_plus, '-' : ball_minus}})



	def get_count_mark(self, val, mark):
		''' 
		Количество строк, помеченных mark 
		----------------------------------------------------------------
		* Args: val = list, mark = list, tuple
		----------------------------------------------------------------
		* return: dict	 
		'''
		if isinstance(mark,(tuple,list)):
			c = 0
			count = {m: 0 for m in mark}
			count_c = [0 for m in mark]
			for v in val:
				for i,m in enumerate(mark):
					if "+" in m or "-" in m:
						m_esc = '\\' + m
					else:
						m_esc = m

					if re.search(m_esc, str(v)):
						count_c[i] += 1 #это баллы
						count.update({m : count_c[i]})
			return count
		else: return None


	
	def format_endblock(self, head_block, question_number):
		'''
		form dict: {parameters:dict|string, question : string, answers : list}
		Вставка картинок
		----------------------------------------------------------------
		* Args: list
		----------------------------------------------------------------
		* return: dict	 
		'''
		
		#print(head_block, '\n\n\n')

		quest_text, prev_key = '', ''
		ans_var = []
		ans_image_file = []
		quest_type = head_block.pop(0)
		ans_ball = head_block.pop(0)
		question_ball = self.QUESTION_BALL
		
		# TODO q_image_file не определяется временами
		#q_image_file = None
		for val in head_block:
			
			#print(val)
			if 'B:' in val:
				question_ball = val['B:']
		
			#print(question_ball, '\n\n')
			#print(val.setdefault(), '\n\n')
						
			if 'S:' in val or 'Q:' in val:
				key = 'Q:' if 'Q:' in val else 'S:'
				quest_text = '<p>{0}</p>'.format(val[key])
				
				if 'S:' in val and 'Q:' in val:
					quest_text = '<p>{0}</p><p>{1}</p>'.format(val['Q:'], val['S:'])
				
				quest_text_proc = self.set_img(quest_text)
				if quest_text_proc : # if not quest_text_proc is None
					quest_text = quest_text_proc[0]
					q_image_file = quest_text_proc[1]
				else:
					q_image_file = None
		
			#if 'I:' not in val not in
			
			if 'I:' not in val and 'S:' not in val and 'Q:' not in val and 'B:' not in val:
				
				a_val = self.get_from_dict(val, 'value')
				a_key = self.get_from_dict(val, 'key')
				
				print('AVAL', a_val, '\n\n')
				a_val_proc = self.set_img(a_val)
				print('AVAL', a_val, '\n\n')
				if a_val_proc:
					#if 'test1' in a_val:
						#print('test test test')
						#print(a_val_proc)
					
					#a_val = a_val_proc[0]
					a_val = a_val_proc[0]# if len(a_val) == 1 else a_val_proc
					a_image_file = a_val_proc[1]# if len(a_val_proc) == 1 else a_val_proc
					#print('\n\n AIMAGEFILE', a_image_file)
					print('\n\n a_val', a_val)
					#a_image_file = a_val_proc[1]
					
					
				else:
					a_image_file = None
				
				if quest_type['question_type'] == 'shortanswer':
	
					#print(a_val)
					a_val = re.sub(r'\<[^>]*\>', '', a_val.strip())
					#print(a_val, '\n\n')
				#ans_var.append({a_key[0] : a_val[0], 'image' : a_image_file})
				ans_var.append(({a_key : a_val}, a_image_file))
				ans_image_file.append((a_key, a_image_file))
				
		#print(q_image_file)
		#print(type(q_image_file))
		block_d = {
			'question_text': quest_text,
			'question_number': question_number,
			'question_ball': question_ball,
			'answers_text' : ans_var,	
			#'a_image_file' : ans_image_file,
			'q_image_file' : q_image_file		
		}
		
		#print(block_d['answers_text'])
		
		try:
			block_d.update(quest_type)
			block_d.update(ans_ball)
			#print(block_d)
			#block_d.update(image_file)
			
		
			#print(block_d['answers_text'],block_d['a_image_file'], '\n\n')
		except ValueError:
			#err = {'type' : 'ValueError', 'function' : 'format_endblock', 'text' : head_block}
			self.Files.put_log('Ошибка при формирование окончательного варианта блока "{0}"?'.format(head_block))
			block_d = None
		
		return block_d
		


	def set_img(self, val):
		''' 
		Загрузка картинок 
		----------------------------------------------------------------
		* Args: val = string
		*		flag = замена картинок в соответствии с self.img_rels
		*				(docx)
						(html)- простая замена
		----------------------------------------------------------------
		* return: string	 
		'''
		
		if self.MODE == '.docx':
			#print(val)
			pattern = re.compile(r'@[\w]+@')
			#pattern_cx = re.compile(r'@cx[0-9.]+@')
		
			list_img = pattern.findall(val)
			
			img_param = []
			if list_img:
				val2 = val
				for found_id in list_img:
				#здесь накапливаем	
					for rels_id, rels_target in self.img_rels:
						
						if found_id[1:-1] == rels_id:
							
							img_name = rels_target.split('/') #-1 - последняя часть
							#print(img_name[-1], rels_target)
							#img_file = open(self.Files.path_input + self.Files.path_project + 'word/' + rels_target, 'rb')
							img_file = open(rels_target, 'rb')
							img_file64 = base64.b64encode(img_file.read())
							#print(img_file, img_file64)
							if self.MODE_CMD == 1:
								img_in_text = '&nbsp;<img src="@@PLUGINFILE@@/{0}" alt="{1}" />&nbsp;'.format(img_name[-1],img_name[-1][0:-4], self.img_size[found_id[1:-1]]['cy'], self.img_size[found_id[1:-1]]['cx'])
							else:
								img_in_text = '&nbsp;<img src="@@PLUGINFILE@@/{0}" alt="{1}" height="{2}" width="{3}" />&nbsp;'.format(img_name[-1],img_name[-1][0:-4], self.img_size[found_id[1:-1]]['cy'], self.img_size[found_id[1:-1]]['cx'])
							#img_in_text = '&nbsp;<img src="@@PLUGINFILE@@/{0}" alt="{1}" style="height: {2}mm; width:{3}mm;1" />&nbsp;'.format(img_name[-1],img_name[-1][0:-4], self.img_size[found_id[1:-1]]['cy'], self.img_size[found_id[1:-1]]['cx'])
							val = val.replace(found_id, img_in_text)
							#print(val, '\n\n')
							#img_param = {'name' : img_name[-1], 'path' : '/', 'encoding' : 'base64'}
							img_param.append((img_name[-1], img_file64)) 
							#print(img_name[-1],'\n\n', img_file64)
				return (val, img_param)
				
		elif self.MODE == '.html':
			
			#val1 = re.sub(r"^\s+|\n|\r|\s+$", '', val)

			pattern = re.compile(r'@[\w,/,\-,\.]{,4096}@')
			list_img = pattern.findall(val)
			img_param = []
			res_val, res_img_param = [], []
			if list_img:
				val2 = val
				print('\n\n LST:',list_img)
				print('\n\n VAL:', val)
				for k, found_id in enumerate(list_img):
				#здесь накапливаем
					print(found_id)
					img_name = 'image' + str(k)
					img_file = open(found_id[1:-1], 'rb')
					img_file64 = base64.b64encode(img_file.read())
					img_in_text = '<img src="@@PLUGINFILE@@/{0}" alt="{1}"/>'.format(img_name,img_name)
					#print(img_in_text)
					val = val.replace(found_id, img_in_text)
					print('\n\n VAL 2:', val)
					img_param.append((img_name, img_file64)) 
					
					#print(img_param)
					#print(val, img_param)
				res_val.append(val)
				res_img_param.append(img_param)
				return (val, img_param)
				
				
			



	def set_img_encoding(self, val):
		''' 
		Загрузка картинок без предварительной подготовки self.img_rels
		----------------------------------------------------------------
		* Args: val = string
		*		flag = замена картинок в соответствии с self.img_rels
		*				(docx)
						(html)- простая замена
		----------------------------------------------------------------
		* return: string	 
		'''
		pattern = re.compile(r'@[\w]+@')
		list_img = pattern.findall(val)
		img_param = []
		if list_img:
				val2 = val
				for found_id in list_img:
				#здесь накапливаем	
					img_name = 'image'
					#img_file = open(self.Files.path_input + self.Files.path_project + 'word/' + rels_target, 'rb')
					img_file = open(val, 'rb')
					img_file64 = base64.b64encode(img_file.read())
					img_in_text = '<img src="@@PLUGINFILE@@/{0}" alt="{1}"/>'.format(img_name,img_name)
					val = val.replace(found_id, img_in_text)
					#img_param = {'name' : img_name[-1], 'path' : '/', 'encoding' : 'base64'}
					img_param.append((img_name, img_file64)) 
			#print(val)			
				return (val, img_param)
			


	def text_formatting(self, val):
		''' 
		Форматирование текста (цвет, шрифт, индекс и т.п.)
		Пока что только индекс
		----------------------------------------------------------------
		* Args: val = list, mark = list, tuple
		----------------------------------------------------------------
		* return: list(dict)	
		* structure: V2: I: Q: S: -:\+:\R1:L1: 
		'''
		struct = re.compile(r'V[\d]:|I:|Q:|S:| \+:|-:|R[\d]:|L[\d]:')
		for k,v in enumerate(arr):
			pass

class FormatAST():
	'''
	Форматирование АСТ
	'''

	
	def timer(f):
		'''
		Время выполнения функций
		----------------------------------------------------------------
		* Args:
		----------------------------------------------------------------
		* return: None	 
		'''
		def tmp(*args, **kwargs):
			t = time.time()
			res = f(*args, **kwargs)
			print("Время выполнения функции: %f" % (time.time()-t))
			return res

		return tmp
		
	
	@timer
	def __init__(self, oFiles):
		'''
		path_project = путь к папке
		----------------------------------------------------------------
		* return: list	 
		'''
		self.Files = oFiles
		document = Document(self.Files.name_file_docx)
		
		sections = document.sections
		paragraphs = document.paragraphs
		
