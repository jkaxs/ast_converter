#! /usr/bin/env python
# -*- coding: utf-8 -*-

import time, re, os, sys, shutil, string, random
import lxml.etree as etree
import inspect #для вызова имени вызываемой функции
from zipfile import ZipFile
from datetime import datetime

from docx import Document #python docx


class Files:
	'''
	--------------------------------------------------------------------
	Создание, удаление файлов и директорий
	--------------------------------------------------------------------
	'''
	#path_output = 'file-output/'
	path_input = 'file-input/'
	#path_project = ''
	def __init__(self, project=''):
		'''-------------------------------------------------------------
		
		----------------------------------------------------------------
		* return: None	 
		'''
		self.logs = [] #логи
		self.path_output = 'file-output/'
		self.path_input = 'file-input/'
		self.path_project = '' #папка с распакованным docx
		self.target_resource = '' # целевой ресурс, который передаем formatDocx
		
		if self.file_path_exists(project):
			self.name_file_docx = project #имя файла Word remove
			self.base_path_file = os.path.abspath(project) #абсолютный путь к файлу
			self.file_name = os.path.basename(self.base_path_file) #имя рабочего файла
			self.base_path_subdir = self.base_path_file [0: len(self.base_path_file) - len(self.file_name)] #абсолютный путь к папке файла
			self.file_name_without_ext, self.file_extension = os.path.splitext(self.file_name)

		else:
			raise SystemExit
	
	
	def file_path_exists(self, path):
		'''-------------------------------------------------------------
		Запись в файл
		----------------------------------------------------------------
		* Args: string, string
		----------------------------------------------------------------
		* return: None	 
		'''
		
		try:
			f = open(path)
			return True
		except OSError as e:
			self.put_log('OSError({0}): {1} в "{2}"'.format(e.errno, e.strerror, path))
		except:
			self.put_log(' Путь: {1}, неожиданная ошибка {0}:'.format(sys.exc_info()[0], path))
			raise SystemExit #?
			
	
	def file_del(self, filepath):
		'''-------------------------------------------------------------
		удаление
		----------------------------------------------------------------
		* Args: string, string, string
		----------------------------------------------------------------
		* return: None	 
		'''
		
		try:
			if os.path.isfile(filepath):
				os.remove(filepath)
			if os.path.isdir(filepath):
				shutil.rmtree(filepath)

		except OSError as e:
			self.put_log('OSError({0}): {1} в "{2}"'.format(e.errno, e.strerror, path))
		except:
			self.put_log(' Путь: {1}, неожиданная ошибка {0}:'.format(sys.exc_info()[0], path))
			raise SystemExit #?	


	
	def randstring(self, n):
		'''-------------------------------------------------------------
		randstring
		----------------------------------------------------------------
		* Args: int
		----------------------------------------------------------------
		* return: None	 
		'''

		a = string.ascii_letters + string.digits
		return ''.join([random.choice(a) for i in range(n)])
		

	def zip_format(self):
		'''-------------------------------------------------------------
		Распаковка файла в архив, файл удаляется.
		path_project = папка с распакованным docx
		----------------------------------------------------------------
		* Args: 
		----------------------------------------------------------------
		* return: None	 
		'''
		zip_file = '{0}.zip'.format(self.file_name_without_ext)

		if self.file_extension != '.docx':
			self.put_log('Расширение файла {0} не docx'.format(self.base_path_file))
			raise SystemExit
		try:
			self.target_resource = '{0}{1}/'.format(self.base_path_subdir, self.file_name_without_ext)
			os.rename(self.base_path_file, '{0}{1}'.format(self.base_path_subdir, zip_file))
			
			z = ZipFile('{0}{1}'.format(self.base_path_subdir, zip_file), 'r')
			z.extractall(self.target_resource) #? 
			z.close()
			
			#self.file_del('{0}{1}'.format(self.base_path_subdir, zip_file)) # zip
		except:
			self.put_log(' Неожиданная ошибка {0}:'.format(sys.exc_info()[0]))
			raise SystemExit
	
	

	
	def file_write(self, path, stri):
		'''-------------------------------------------------------------
		Запись в файл
		----------------------------------------------------------------
		* Args: string, string
		----------------------------------------------------------------
		* return: None	 
		'''
		try:
			print(path)
			outFile = open(path, 'w')
			outFile.write(stri)
			outFile.close
		except TypeError:
			print("file_write: stri должна быть строкой.")
		except FileNotFoundError:
			print('file_write: не найдена папка.')


	
	def put_log(self, text, flag='error'):
		'''-------------------------------------------------------------
		Ведение логов
		----------------------------------------------------------------
		* Args: string, string
		flag = 'error' - учет ошибок
			 = 'log' ведение логов + исключаемые вопросы
		----------------------------------------------------------------
		* return: None	 
		'''
		time = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")
		func_name = inspect.stack()[1][3]
		
		if flag == 'error':
			stri = 'ERR: {0}, {1}, {2}'.format(time, func_name, text)
		elif flag == 'log':
			stri = 'Время: {0}; {1}'.format(time, text)
		print(stri)
		self.logs.append(stri)


########################################################################
	
class MoodleXML():
	'''
	--------------------------------------------------------------------
	[{'I:': text}, {'Q:' : text}, {'S:':text}, {'+:':text}, # + (т.п.),
	{'ans_ball': {'+': int, '-':int|None}}, {'quest_type' : string|None}
	--------------------------------------------------------------------
	'''

	def __init__(self, category_text, Files):
		'''-------------------------------------------------------------
		
		----------------------------------------------------------------
		* return: None	 
		'''
		#super(MoodleXML, self).__init__()
		dtime = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M")		
		self.test_file = '{0}{1}.xml'.format(Files.path_output, category_text)
				
		# default:
		self.parameters = {
			'question' : {
				'format':'html'
				},
			'answer' : {
				'format':'html',
				'shuffleanswers' : '1', #перемешать ответы
				'answernumbering' : 'abc' #нумерация
				} 
			}

		self.all_document = ''
		self.preambula = '<?xml version="1.0" ?>\n' # не нужно
		
		#cat = '$course$/{0}'.format(category_text)
		cat = '{0}'.format(category_text)
		
		self.quiz = etree.Element('quiz')
		
		#для раздельных файлов
		#self.put_category(cat)

	
	def put_text(self, base_element, text):
		'''-------------------------------------------------------------
		<text> </text> одну строчку
		----------------------------------------------------------------
		* Args: element xml, string
		----------------------------------------------------------------
		* return: None	 
		''' 
		t = etree.SubElement(base_element, 'text')
		t.text = text
	
	
	def put_file_img(self, base_element, img_block):
		'''-------------------------------------------------------------
		Вставка элемента с картинкой, кодирование base64
		----------------------------------------------------------------
		* Args: element xml, list[str,byte]
		img_block[0] = name
		img_block[1] = b'img
		----------------------------------------------------------------
		* return: None	 
		''' 
		for img in img_block:
			i = etree.SubElement(base_element, 'file', name = img[0],  path="/", encoding="base64")
			i.text = img[1]
			
		
	def put_question(self, q_type, quest_text, question_number, question_ball, img = None):
		'''-------------------------------------------------------------
		flag_single (multichoice):
			1 - один из многих
			2 - многие из многих
		----------------------------------------------------------------
		* Args: str, str, int
		----------------------------------------------------------------
		* return: xml	 
		'''
		quest = etree.SubElement(self.quiz, 'question', type=q_type)
		name = etree.SubElement(quest, 'name')
		self.put_text(name, str(question_number))
		
		questiontext = etree.SubElement(quest, 'questiontext', format=self.parameters['question']['format'])
		self.put_text(questiontext, quest_text)
		
		#балл за ответ
		defaultgrade = etree.SubElement(quest, 'defaultgrade')
		#defaultgrade.text = '6.0000000'
		defaultgrade.text = '{0}.0000000'.format(question_ball)
		#print(defaultgrade.text)
		
		if img:
			self.put_file_img(questiontext, img)
		
		return quest
	
		
	def put_category(self, text):
		'''-------------------------------------------------------------
		Вставка категории
		----------------------------------------------------------------
		* Args: str
		----------------------------------------------------------------
		* return: none	 
		'''
		quest = etree.SubElement(self.quiz, 'question', type="category")
		category = etree.SubElement(quest, 'category')
		self.put_text(category, text)
	

	
	def put_answer(self, quest_element, ans_text, ans_ball = '', flag_fraction = True, img = None):
		'''-------------------------------------------------------------
		doc
		----------------------------------------------------------------
		* Args: xml element, string, dict, bool, img = list[name, filecontent]
			flag_fraction:
			True - есть оценка
			False - нет оценки
		----------------------------------------------------------------
		* return: xml element	 
		'''
		if flag_fraction == True:
			answer = etree.SubElement(quest_element, 'answer', fraction=ans_ball)
		else:
			answer = etree.SubElement(quest_element, 'answer')
			
		self.put_text(answer, ans_text)
		if img:
			self.put_file_img(answer, img)
		
		return answer
	

	
	def process_data(self, block, cat):
		'''-------------------------------------------------------------
		doc
		----------------------------------------------------------------
		* Args: list
		----------------------------------------------------------------
		* return: None	 
		'''
		if type(block) is dict:
			
			#self.put_category(cat)
	
			quest = self.put_question(block['question_type'], block['question_text'], block['question_number'], block['question_ball'], block['q_image_file'])
			#answers
			#общее для всех типов вопросов
			for ans in block['answers_text']:
				#print('ANS', ans)
				for k, v in ans[0].items():
					ans_key, ans_text = k.strip(), v.strip()
				img = ans[1]
				#print(img)
				
				#mathing
				if block['question_type'] == 'matching':
					if ans_key[0] == 'L':
						subquestion = etree.SubElement(quest, 'subquestion')
						self.put_text(subquestion, ans_text)
						if img:
							self.put_file_img(subquestion, img)
						
						number_L = re.search(r'[\d]', ans_key)
						R_key = 'R' + number_L.group(0) + ':'
					
						for r in block['answers_text']:
							if R_key in r[0]: R_val = r[0]
						self.put_answer(subquestion, R_val[R_key].strip(), flag_fraction = False)
						
				#!mathing
				else:
				
					ball = block['answers_ball']['+'] if '+' in ans_key	else block['answers_ball']['-']
					self.put_answer(quest, ans_text.strip(), str(ball), img = ans[1])

					#if img:
					#	self.put_file_img(quest, img)
					
					#postambula
					if block['question_type'] == 'multichoice': 
						# один из многих
						flag_single = 'true' if block['answers_ball']['+'] == 100 else 'false'
						
						shuffleanswers = etree.SubElement(quest, 'shuffleanswers')
						single = etree.SubElement(quest, 'single')
						answernumbering = etree.SubElement(quest, 'answernumbering')
						
						shuffleanswers.text = self.parameters['answer']['shuffleanswers']
						single.text = flag_single
						answernumbering.text = self.parameters['answer']['answernumbering']
			
		else:
			Files.put_log('Неправильно сформированы данные')
			raise SystemExit	
								

	
	def write_xml(self):
		'''-------------------------------------------------------------
		doc
		----------------------------------------------------------------
		* Args:
		----------------------------------------------------------------
		* return: None	 
		'''
		try:
			stri = etree.tostring(self.quiz, method = 'xml', pretty_print=True, xml_declaration=True, encoding = 'utf-8')
			Files.file_write(self, self.test_file, stri.decode('utf-8'))
		except Exception:
			Files.put_log('Не получилось записать {0}'.format(obj))
			raise SystemExit

		#print('xml:',stri)
	
	
	def write_xml_all(self, list_cat):
		'''-------------------------------------------------------------
		Запись в один файл
		----------------------------------------------------------------
		* Args: Список объектов
		----------------------------------------------------------------
		* return: None	 
		'''
		try:
			stri = etree.tostring(list_cat, method = 'xml', pretty_print=True, xml_declaration=True, encoding = 'utf-8')
			Files.file_write(self, self.test_file, stri.decode('utf-8'))
		except AttributeError:
			Files.put_log('Не получилось записать {0}'.format(obj))
			raise SystemExit
	

		#print('xml:',stri)


########################################################################	
class Gift:
	'''
	--------------------------------------------------------------------
	
	--------------------------------------------------------------------
	'''
	def __init__(self, list_text):
		'''-------------------------------------------------------------
		
		----------------------------------------------------------------
		* return: list	 
		'''
		pass
